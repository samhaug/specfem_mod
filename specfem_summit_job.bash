#!/bin/bash
# LSF scheduler options
#BSUB -P GEO111
#BSUB -J test
#BSUB -o job.output
#BSUB -e job.error

# 30min walltime
#BSUB -W 00:30

#BSUB -nnodes 64


NPROC=384

###################################################

STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"

module unload darshan-runtime
# export OMPI_MCA_io=romio321
# export ROMIO_HINTS=./my_romio_hints


echo "running simulation: `date`"
echo "directory: `pwd`"
echo
module list
echo

# runs mesher
echo
echo "running mesher..."
echo `date`
jsrun -n384 -r6 -g1 -a1 -c1 ./bin/xmeshfem3D
if [[ $? -ne 0 ]]; then exit 1; fi

# runs simulation
echo
echo "running solver..." 
echo `date`
jsrun -n384 -r6 -g1 -a1 -c1 ./bin/xspecfem3D
if [[ $? -ne 0 ]]; then exit 1; fi

echo
echo "see results in directory: OUTPUT_FILES/"

echo
echo "done: `date`"
ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."
