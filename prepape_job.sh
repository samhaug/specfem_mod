#!/bin/sh

# Run this in Specfem Folder

root_dir=/gpfs/alpine/geo111/scratch/haugland

name=$1

if [[ -z $name ]]; then
    echo "No job name is given."
    exit
fi

target_dir=${root_dir}/$name

if [[ -d $target_dir ]]; then
    echo "Folder exists!"
    read -p "Do you want to delete it (y/n)? " answer
    case ${answer:0:1} in
	y|Y )
            echo Deleting previous folder...
	    rm -rf $target_dir
	    ;;
	* )
            echo Exiting...
	    exit
	    ;;
    esac
fi

CURDIR=`pwd`

mkdir -p $target_dir
cd $target_dir
mkdir -p DATA DATABASES_MPI OUTPUT_FILES
#cp -r $CURDIR/bin.kernel bin
cp -r $CURDIR/bin bin
cd DATA
echo "Copying parameter files..."
cp -r $CURDIR/DATA/{Par_file,STATIONS,CMTSOLUTION} .
echo "Linking model files..."
ln -s $root_dir/DATA/cemRequest .
ln -s $root_dir/DATA/crust1.0 .
ln -s $root_dir/DATA/crust2.0 .
ln -s $root_dir/DATA/crustmap .
ln -s $root_dir/DATA/epcrust .
ln -s $root_dir/DATA/eucrust-07 .
ln -s $root_dir/DATA/full_sphericalharmonic_model .
ln -s $root_dir/DATA/heterogen .
ln -s $root_dir/DATA/Lebedev_sea99 .
ln -s $root_dir/DATA/Montagner_model . 
ln -s $root_dir/DATA/old .
ln -s $root_dir/DATA/PPM .
ln -s $root_dir/DATA/QRFSI12 .
ln -s $root_dir/DATA/s20rts .
ln -s $root_dir/DATA/s362ani .
ln -s $root_dir/DATA/s40rts .
ln -s $root_dir/DATA/sglobe .
ln -s $root_dir/DATA/Simons_model .
ln -s $root_dir/DATA/topo_bathy .
ln -s $root_dir/DATA/Zhao_JP_model .

if grep -wq gll_azi $CURDIR/DATA/Par_file; then
    echo "linking GLL_AZI"
    ln -s $root_dir/DATA/GLL_AZI ./GLL
fi

if grep -wq gll $CURDIR/DATA/Par_file; then
    echo "linking GLL_M15"
    ln -s $root_dir/DATA/GLL_M15 ./GLL
fi

#read -p "Do you want to M25 (1) or M15 (0)?" answer
#case ${answer:0:1} in
#	1 )
#        echo Linking M25 as GLL...
#	    ln -s $root_dir/DATA/model_M25.blend GLL
#	    ;;
#	0 )
#        echo Linking M25 as GLL...
#	    ln -s $root_dir/DATA/M15 GLL
#	    ;;
#	* )
#        echo NO GLL ModelExiting...
#	    exit
#	    ;;
#esac
#
#

echo
nproc_xi=`grep NPROC_XI Par_file | cut -d "=" -f 2`
nproc_eta=`grep NPROC_ETA Par_file | cut -d "=" -f 2`
nchunks=`grep NCHUNKS Par_file | cut -d "=" -f 2`
NPROC=$((nproc_xi*nproc_eta*nchunks))
echo NPROC=$NPROC
NUMBER_OF_NODES=`python -c "import math; print int(math.ceil($NPROC/6.0))"`
echo nodes=$NUMBER_OF_NODES

cd ..

cp $CURDIR/{change_simulation_type.pl,specfem_summit_job_gpumps.bash,.my_romio_hints} .
sed -i "s/:NUMBER_OF_NODES:/$NUMBER_OF_NODES/g" specfem_summit_job_gpumps.bash
sed -i "s/:NUMBER_OF_NODES:/$NUMBER_OF_NODES/g" .my_romio_hints
sed -i "s/:NPROC:/$NPROC/g" specfem_summit_job_gpumps.bash
sed -i "s/:NCHUNKS:/$nchunks/g" specfem_summit_job_gpumps.bash
sed -i "s/:NAME:/$name/g" specfem_summit_job_gpumps.bash

echo You can submit the job using:
echo cd $target_dir
echo bsub $target_dir/specfem_summit_job_gpumps.sh
