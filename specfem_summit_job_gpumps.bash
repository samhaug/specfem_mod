#!/bin/bash
# LSF scheduler options
#BSUB -P GEO111
#BSUB -J :NAME:
#BSUB -o job.output.%J
#BSUB -e job.error.%J

#BSUB -W 00:50

#BSUB -nnodes 16
#BSUB -alloc_flags gpumps  # gpu multiple MPI process per GPU


NPROC=:NPROC:

###################################################

STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"

module unload darshan-runtime
# export OMPI_MCA_io=romio321
# export ROMIO_HINTS=./my_romio_hints


echo "running simulation: `date`"
echo "directory: `pwd`"
echo
module list
echo

# runs mesher
# echo
# echo "running mesher..."
# echo `date`
jsrun -n96 -r6 -g1 -a4 -c4 ./bin/xmeshfem3D
if [[ $? -ne 0 ]]; then exit 1; fi

# ./change_simulation_type.pl -F

# runs simulation
echo
echo "running solver..." 
echo `date`
jsrun -n96 -r6 -g1 -a4 -c4 ./bin/xspecfem3D
if [[ $? -ne 0 ]]; then exit 1; fi

echo
echo "see results in directory: OUTPUT_FILES/"

echo
echo "done: `date`"
ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."
