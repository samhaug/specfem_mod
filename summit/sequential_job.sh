#!/bin/bash
# LSF scheduler options
#BSUB -P GEO111
#BSUB -J m25_4batch
#BSUB -o job.output.%J
#BSUB -e job.error.%J

# 30min walltime
#BSUB -W 01:00

#BSUB -nnodes 3200


NPROC=19200

###################################################

STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"

module unload darshan-runtime
# export OMPI_MCA_io=romio321
# export ROMIO_HINTS=./my_romio_hints


echo "running multiple simulations: `date`"
echo
module list
echo


root=`pwd`
echo "RUNNING JOB1"
cd ./m25_batch01
sh specfem_summit_subjob.bash
cd $root


echo "RUNNING JOB2"
cd ./m25_batch02
sh specfem_summit_subjob.bash
cd $root

echo "RUNNING JOB3"
cd ./m25_batch03
sh specfem_summit_subjob.bash
cd $root

echo "RUNNING JOB4"
cd ./m25_batch04
sh specfem_summit_subjob.bash
cd $root

echo
echo "done: `date`"
ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."

