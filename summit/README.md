# Summit

Modules

Currently Loaded Modules:
  1) xl/16.1.1-3   2) spectrum-mpi/10.3.0.1-20190611   3) hsi/5.0.2.p5   4) xalt/1.1.3   5) lsf-tools/2.0   6) darshan-runtime/3.1.7   7) DefApps   8) cuda/10.1.168   9) cmake/3.14.2  10) boost/1.59.0  11) hdf5/1.8.18  12) emacs/25.1


- prepape_job.sh: copies data files into scratch
- specfem_summit_job.bash: main job script
- specfem_summit_job_gpumps.bash:  job script with gpumps option on which enables submitting 4 task per gpu