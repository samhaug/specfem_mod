#!/bin/sh

# Prepare batch script for SUMMIT
# Run this in Specfem Folder

root_dir=/gpfs/alpine/geo111/scratch/$USER
name=$1

if [[ -z $name ]]; then
    echo "No job name is given."
    exit
fi

target_dir=${root_dir}/$name

if [[ -d $target_dir ]]; then
    echo "Folder exists!"
    read -p "Do you want to delete it (y/n)? " answer
    case ${answer:0:1} in
	y|Y )
            echo Deleting previous folder...
	    rm -rf $target_dir
	    ;;
	* )
            echo Exiting...
	    exit
	    ;;
    esac
fi

CURDIR=`pwd`

mkdir -p $target_dir
cd $target_dir
mkdir -p DATA DATABASES_MPI OUTPUT_FILES
# cp -r $CURDIR/bin.kernel bin
cp -r $CURDIR/bin bin

cd DATA
echo "Copying parameter files..."
cp -r $CURDIR/DATA/{Par_file,STATIONS,CMTSOLUTION} .
echo "Linking model files..."
ln -s $root_dir/DATA/cemRequest .
ln -s $root_dir/DATA/crust1.0 .
ln -s $root_dir/DATA/crust2.0 .
ln -s $root_dir/DATA/crustmap .
ln -s $root_dir/DATA/epcrust .
ln -s $root_dir/DATA/eucrust-07 .
ln -s $root_dir/DATA/full_sphericalharmonic_model .
ln -s $root_dir/DATA/heterogen .
ln -s $root_dir/DATA/Lebedev_sea99 .
ln -s $root_dir/DATA/Montagner_model . 
ln -s $root_dir/DATA/old .
ln -s $root_dir/DATA/PPM .
ln -s $root_dir/DATA/QRFSI12 .
ln -s $root_dir/DATA/s20rts .
ln -s $root_dir/DATA/s362ani .
ln -s $root_dir/DATA/s40rts .
ln -s $root_dir/DATA/sglobe .
ln -s $root_dir/DATA/Simons_model .
ln -s $root_dir/DATA/topo_bathy .
ln -s $root_dir/DATA/Zhao_JP_model .

IS_MODEL_GLL=`egrep '^MODEL += +GLL' Par_file`
IS_MODEL_GLL_3DQ=`egrep '^MODEL += +GLL_3DQ' Par_file`

if [[ ! -z $IS_MODEL_GLL ]]; then
    read -p "Do you want to use M25 (1) or M15 (0)? " answer
    case ${answer:0:1} in
	1 )
            echo Linking M25 as GLL...
	    ln -s $root_dir/DATA/model_M25.blend GLL
	    cd ../DATABASES_MPI
	    if [[ ! -z $IS_MODEL_GLL_3DQ ]]; then
		echo "Linking 3DQ meshfiles"
		ln -s $root_dir/m25_3dq_mesh/DATABASES_MPI/* .
		cd ..
		cp -r $root_dir/m25_3dq_mesh/OUTPUT_FILES/* OUTPUT_FILES
	    else
		echo "Linking meshfiles"
		ln -s $root_dir/m25_mesh/DATABASES_MPI/* .
		cd ..
		cp -r $root_dir/m25_mesh/OUTPUT_FILES/* OUTPUT_FILES
	    fi
	    cd DATA
	    ;;
	0 )
            echo Linking M15 as GLL...
	    ln -s $root_dir/DATA/M15 GLL
	    cd ../DATABASES_MPI
	    if [[ ! -z $IS_MODEL_GLL_3DQ ]]; then
		echo "Linking 3DQ meshfiles"
		ln -s $root_dir/m15_3dq_mesh/DATABASES_MPI/* .
		cd ..
		cp -r $root_dir/m15_3dq_mesh/OUTPUT_FILES/* OUTPUT_FILES
	    else
		echo "Linking meshfiles"
		ln -s $root_dir/m15_mesh/DATABASES_MPI/* .
		cd ..
		cp -r $root_dir/m15_mesh/OUTPUT_FILES/* OUTPUT_FILES
	    fi
	    cd DATA
	    ;;
	* )
            echo NO GLL Model Exiting...
	    exit
	    ;;
    esac
fi


echo
nproc_xi=`grep NPROC_XI Par_file | cut -d "=" -f 2`
nproc_eta=`grep NPROC_ETA Par_file | cut -d "=" -f 2`
nchunks=`grep NCHUNKS Par_file | cut -d "=" -f 2`
nsimruns=`grep ^NUMBER_OF_SIMULTANEOUS_RUNS Par_file | cut -d "=" -f 2`

NPROC=$((nproc_xi*nproc_eta*nchunks*nsimruns))
gpu_mode=`grep "^GPU_MODE[[:space:]]*=[[:space:]]*.true." Par_file`

if [[ ! -z $gpu_mode ]]; then
    node_size=6
    echo "GPU RUN"
else
    node_size=42
    echo "CPU RUN"
fi

echo NCHUNKS =$nchunks
echo N_SIMULTANEOUS_RUNS =$nsimruns
echo NPROC = $NPROC
NUMBER_OF_NODES=`python2 -c "import math; print int(math.ceil($NPROC/$node_size))"`
echo nodes = $NUMBER_OF_NODES

cd ..

cp $CURDIR/{change_simulation_type.pl,specfem_summit_job.bash} .
sed -i "s/:NUMBER_OF_NODES:/$NUMBER_OF_NODES/g" specfem_summit_job.bash
# sed -i "s/:NUMBER_OF_NODES:/$NUMBER_OF_NODES/g" .my_romio_hints
sed -i "s/:NPROC:/$NPROC/g" specfem_summit_job.bash
sed -i "s/:NCHUNKS:/$nchunks/g" specfem_summit_job.bash
sed -i "s/:NAME:/$name/g" specfem_summit_job.bash

if [[ -z $gpu_mode ]]; then
    sed -i "s/\-r6 \-g1 \-a1 \-c1//g" specfem_summit_job.bash
fi

echo You can submit the job using:
echo cd $target_dir
echo bsub $target_dir/specfem_summit_job.bash
