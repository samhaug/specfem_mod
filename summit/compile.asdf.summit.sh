#!/bin/sh

rm -rf build
mkdir -p build

cd build
cmake .. -DCMAKE_Fortran_COMPILER=mpif90 -DCMAKE_C_COMPILER=mpicc -DCMAKE_CXX_COMPILER=mpicxx \
      -DCMAKE_Fortran_FLAGS="-qextname"
make
